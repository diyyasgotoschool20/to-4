package com.company;
public class pesanan extends kue {
//subclass dari kelas kue

    private double berat;  //variabel untuk menyimpan berat kue

    //konstruktor
    public pesanan(String name, double price, double berat) {
        //memanggil konstruktor dari super class (kue)
        super(name, price);
        //inisialisasi variabel berat
        this.berat = berat;
    }
    // method getter untuk mengembalikan nilai berat kue
    public double getBerat() {
        return berat;
    }
    //method setter untuk mengubah nilai berat kue
    public void setBerat(double berat) {
        this.berat = berat;
    }
    //implementasi method abstrak hitungHarga() untuk menghitung harga kue
    //menghitung harga kue berdasarkan harga per kg dikalikanberat kue
    @Override
    public double hitungHarga() {
        return getPrice() * berat;
    }
    //mengembalikan nilai berat kue
    @Override
    public double Berat() {
        return berat;
    }
    //sama dengan method hitungHarga
    @Override
    public double Jumlah() {
        return hitungHarga();
    }
    //override method toString() dari superclass kue
    @Override
    public String toString() {
        return super.toString() + String.format("\nBerat\t\t: %.2f kg\nJumlah\t\t: %.2f", berat, Jumlah());
    }
}

