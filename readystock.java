package com.company;
public class readystock extends kue {
    // kelas readystock merupakan subclass dari kelas kue
    private double jumlah;

    // konstruktor yang menginisialisasi nama, harga, dan jumlah
    public readystock(String name, double price, double jumlah) {
        // memanggil konstruktor dari super class (kue)
        super(name, price);
        // inisialisasi variabel jumlah
        this.jumlah = jumlah;
    }

    // method getter untuk mengembalikan nilai jumlah kue
    public double getJumlah() {
        return jumlah;
    }

    // method setter untuk mengubah nilai jumlah kue
    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    // implementasi method abstrak hitungHarga untuk menghitung harga kue
    // menggunakan formula harga x jumlah x 2
    @Override
    public double hitungHarga() {
        return getPrice() * jumlah * 2;
    }

    // implementasi method abstrak Berat untuk menghitung berat kue
    // kue ready stock tidak mempunyai berat sehingga return 0
    @Override
    public double Berat() {
        return 0;
    }

    // implementasi method abstrak Jumlah untuk menghitung jumlah kue
    // sama dengan method hitungHarga
    @Override
    public double Jumlah() {
        return hitungHarga();
    }

    // override method toString untuk mencetak informasi kue
    @Override
    public String toString() {
        return super.toString() + String.format("\nJumlah\t\t: %.2f\nHarga total\t: %.2f", jumlah, Jumlah());
    }
}

